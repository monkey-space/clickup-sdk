import { ClickupConnector } from './../src/utils/clickup-class';

describe('blah', () => {
  it('works', async () => {
    const apiKey = '<api-key>';
    const blockchainWorkId = '<user-id>';
    //const meUserId = '3055456';
    const clickupClient = new ClickupConnector(apiKey);

    // const userId = '3055456'
    const { teams }: any[] | any = await clickupClient.teams.getTeams();
    console.log(teams);

    const {
      shared,
    }: any = await clickupClient.sharedHierarchy.getSharedHierarchy(
      blockchainWorkId
    );
    console.log(shared.folders);
    const list = await clickupClient.spaces.getSpaces(blockchainWorkId);
    console.log(list);
  });
});
