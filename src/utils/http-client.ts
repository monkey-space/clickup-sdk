import { BASE_URL } from './../constants';
import FormData from 'form-data';
import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  Method,
} from 'axios';
import { QueryOptions } from './params';
import { DEFAULT_TIMEOUT } from '../constants';

export class Connector {
  private readonly baseURL = BASE_URL;
  private instance: AxiosInstance;
  private endpoint: string = '';
  private queryParams?: QueryOptions;
  private headers = {
    'Content-Type': 'application/json',
  };

  constructor(private apiKey?: string) {
    this.instance = axios.create({
      baseURL: this.baseURL,
      timeout: DEFAULT_TIMEOUT,
    });
    this.instance.interceptors.request.use((request: AxiosRequestConfig) => {
      if (this.apiKey)
        Object.assign(this.headers, { Authorization: this.apiKey });
      if (this.queryParams) request.params = this.queryParams;
      request.headers = this.headers;
      return request;
    });
    this.instance.interceptors.response.use((response: AxiosResponse) => {
      try {
        return response.data;
      } catch (error) {
        throw error;
      }
    });
  }

  public setApiKey(key: string) {
    this.apiKey = key;
    return this;
  }

  public setQueryParams(params: QueryOptions) {
    this.queryParams = params;
    return this;
  }

  route(url: string | string[]) {
    if (Array.isArray(url)) url = url.join('/');
    this.endpoint = `/${url}`;
    return this;
  }

  request<T, R>(
    method: Method,
    data?: T | FormData | Object | any
  ): Promise<AxiosResponse<R>> {
    const options: AxiosRequestConfig = {
      url: this.endpoint.replace(/\/\/+/g, '/'),
      method,
    };
    if (data) Object.assign(options, { data });
    return this.instance.request<R>(options);
  }
}
