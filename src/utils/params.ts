import { Connector } from './http-client';
export type TypeOperations = string | number | string[] | undefined | boolean;

export abstract class ClickupEndpoint {
  constructor(protected readonly httpClient: Connector) {}
}

export interface QueryOptions {
  [props: string]: TypeOperations;
}
