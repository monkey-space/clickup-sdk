import { Connector } from './http-client';
import { CustomFields } from './../components/CustomFields';
import { Webhooks } from './../components/Webhooks';
import { Views } from './../components/Views';
import { Users } from './../components/Users';
import { TimeTrackingV2 } from './../components/TimeTrackingV2';
import { TimeTrackingLegacy } from './../components/TimeTrackingLegacy';
import { Teams } from './../components/Teams';
import { TaskTemplates } from './../components/TaskTemplates';
import { Tasks } from './../components/Tasks';
import { Tags } from './../components/Tags';
import { Spaces } from './../components/Spaces';
import { SharedHierarchy } from './../components/SharedHierarchy';
import { Members } from './../components/Members';
import { Guests } from './../components/Guests';
import { Goals } from './../components/Goals';
import { Folders } from './../components/Folders';
import { Dependencies } from './../components/Dependencies';
import { Comments } from './../components/Comments';
import { Authorization } from './../components/Authorization';
import { Attachments } from './../components/Attachments';
import { Checklists } from '../components/Checklists';
import { Lists } from '../components/Lists';

export class ClickupConnector {
  private request: Connector;

  public attachments!: Attachments;
  public authorization!: Authorization;
  public checklists!: Checklists;
  public comments!: Comments;
  public customFields!: CustomFields;
  public dependencies!: Dependencies;
  public folders!: Folders;
  public goals!: Goals;
  public guests!: Guests;
  public lists!: Lists;
  public members!: Members;
  public sharedHierarchy!: SharedHierarchy;
  public spaces!: Spaces;
  public tags!: Tags;
  public tasks!: Tasks;
  public taskTemplates!: TaskTemplates;
  public teams!: Teams;
  public timeTracking!: TimeTrackingLegacy;
  public timeTrackingV2!: TimeTrackingV2;
  public users!: Users;
  public views!: Views;
  public webhooks!: Webhooks;

  constructor(apiKey: string) {
    this.request = new Connector(apiKey);
    this.initial();
  }

  setApiKey(key: string) {
    this.request.setApiKey(key);
    this.initial();
  }

  private initial() {
    this.attachments = new Attachments(this.request);
    this.authorization = new Authorization(this.request);
    this.checklists = new Checklists(this.request);
    this.comments = new Comments(this.request);
    this.customFields = new CustomFields(this.request);
    this.dependencies = new Dependencies(this.request);
    this.folders = new Folders(this.request);
    this.goals = new Goals(this.request);
    this.guests = new Guests(this.request);
    this.lists = new Lists(this.request);
    this.members = new Members(this.request);
    this.sharedHierarchy = new SharedHierarchy(this.request);
    this.spaces = new Spaces(this.request);
    this.tags = new Tags(this.request);
    this.tasks = new Tasks(this.request);
    this.taskTemplates = new TaskTemplates(this.request);
    this.teams = new Teams(this.request);
    this.timeTracking = new TimeTrackingLegacy(this.request);
    this.timeTrackingV2 = new TimeTrackingV2(this.request);
    this.users = new Users(this.request);
    this.views = new Views(this.request);
    this.webhooks = new Webhooks(this.request);
  }
}
