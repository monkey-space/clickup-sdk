import { ClickupEndpoint } from '..';

/**
 * @deprecated Class not implemented!
 */
export class Guests extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  inviteGuestToWorkspace() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  editGuestOnWorkspace() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  removeGuestFromWorkspace() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  getGuest() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  addGuestToTask() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  removeGuestFromTask() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  addGuestToList() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  removeGuestFromList() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  addGuestToFolder() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  removeGuestFromFolder() {
    throw new Error('Funtions not implemented!');
  }
}
