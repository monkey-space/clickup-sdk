import { QueryOptions, ClickupEndpoint } from './../utils/params';

export interface AttachmentsParams extends QueryOptions {
  custom_task_ids: string;
  team_id: number;
}

export class Attachments extends ClickupEndpoint {
  createTaskAttachment(
    taskId: string,
    data: FormData,
    query: AttachmentsParams
  ) {
    return this.httpClient
      .route(`/task/${taskId}/attachment`)
      .setQueryParams(query)
      .request('POST', data);
  }
}
