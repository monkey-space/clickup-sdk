import { AttachmentsParams } from './Attachments';
import { ClickupEndpoint } from '../utils';

export class Checklists extends ClickupEndpoint {
  createChecklist(taskId: string, data: FormData, options: AttachmentsParams) {
    return this.httpClient
      .route(`/task/${taskId}/checklist`)
      .setQueryParams(options)
      .request('POST', data);
  }

  editChecklist(checklistId: string, data: FormData) {
    return this.httpClient
      .route(`/checklist/${checklistId}/`)
      .request('PUT', data);
  }

  deleteChecklist(checklistId: string) {
    return this.httpClient
      .route(`/checklist/${checklistId}/`)
      .request('DELETE');
  }

  createChecklistItem(checklistId: string, data: FormData) {
    return this.httpClient
      .route(`/checklist/${checklistId}/checklist_item`)
      .request('POST', data);
  }

  editChecklistItem(
    checklistId: string,
    checklistItemId: string,
    data: FormData
  ) {
    return this.httpClient
      .route(`/checklist/${checklistId}/checklist_item/${checklistItemId}`)
      .request('PUT', data);
  }

  deleteChecklistItem(checklistId: string, checklistItemId: string) {
    return this.httpClient
      .route(`/checklist/${checklistId}/checklist_item/${checklistItemId}`)
      .request('DELETE');
  }
}
