import { ClickupEndpoint } from '../utils';

export class Lists extends ClickupEndpoint {
  createList(folderId: string | number, data: FormData) {
    return this.httpClient
      .route(`/folder/${folderId}/list`)
      .request('POST', data);
  }
  createFolderlessList(spaceId: string | number, data: FormData) {
    return this.httpClient
      .route(`/space/${spaceId}/list`)
      .request('POST', data);
  }
  updateList(listId: string | number, data: FormData) {
    return this.httpClient.route(`/list/${listId}`).request('PUT', data);
  }
  deleteList(listId: string | number) {
    return this.httpClient.route(`/list/${listId}`).request('DELETE');
  }
  getLists(folderId: string | number) {
    return this.httpClient.route(`/folder/${folderId}/list`).request('GET');
  }
  getFolderlessLists(spaceId: string | number) {
    return this.httpClient.route(`/space/${spaceId}/list`).request('GET');
  }
  getList(listId: string | number) {
    return this.httpClient.route(`/list/${listId}`).request('GET');
  }
  addTaskToList(listId: string | number, taskId: string | number) {
    return this.httpClient
      .route(`/list/${listId}/task/${taskId}`)
      .request('POST');
  }
  removeTaskFromList(listId: string | number, taskId: string | number) {
    return this.httpClient
      .route(`/list/${listId}/task/${taskId}`)
      .request('DELETE');
  }
}
