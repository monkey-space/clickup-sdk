import { ClickupEndpoint } from '../utils';

/**
 * @deprecated Funtions not implemented!
 */
export class Views extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  createTeamView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  createSpaceView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  createFolderView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  createListView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  updateView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  deleteView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  getTeamView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  getSpaceView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  getFolderView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  getListView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  getView() {}
  /**
   * @deprecated Funtions not implemented!
   */
  getViewTasks() {}
}
