import { AttachmentsParams } from './Attachments';
import { ClickupEndpoint } from '../utils';

export class Comments extends ClickupEndpoint {
  createTaskComment(taskId: string, data: FormData, query: AttachmentsParams) {
    return this.httpClient
      .route(`/task/${taskId}/comment`)
      .setQueryParams(query)
      .request('POST', data);
  }
  createViewComment(viewId: string, data: FormData) {
    return this.httpClient
      .route(`/view/${viewId}/comment`)
      .request('POST', data);
  }
  createListComment(listId: string, data: FormData) {
    return this.httpClient
      .route(`/list/${listId}/comment`)
      .request('POST', data);
  }
  updateComment(commentId: string, data: FormData) {
    return this.httpClient.route(`/comment/${commentId}`).request('PUT', data);
  }
  deleteComment(commentId: string) {
    return this.httpClient.route(`/comment/${commentId}`).request('DELETE');
  }
  getTaskComment(taskId: string, query: AttachmentsParams) {
    return this.httpClient
      .route(`/task/${taskId}/comment`)
      .setQueryParams(query)
      .request('GET');
  }
  getViewComment(viewId: string) {
    return this.httpClient.route(`/view/${viewId}/comment`).request('GET');
  }
  getListComment(listId: string) {
    return this.httpClient.route(`/list/${listId}/comment`).request('GET');
  }
}
