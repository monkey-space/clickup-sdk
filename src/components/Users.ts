import { QueryOptions, ClickupEndpoint } from './../utils/params';

export interface UsersParams extends QueryOptions {
  client_id: string;
  client_secret: string;
  code: string;
}

/**
 * @deprecated Class not implemented!
 */
export class Users extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  inviteUserToWorkspace() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  editUserOnWorkspace() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  removeUserFromWorkspace() {
    throw new Error('Funtions not implemented!');
  }

  getUser(teamId: number | string, userId: number | string) {
    return this.httpClient
      .route(`/team/${teamId}/user/${userId}`)
      .request('GET');
  }
}
