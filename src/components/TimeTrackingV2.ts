import { QueryOptions, ClickupEndpoint } from './../utils/params';
import { AttachmentsParams } from './Attachments';

export interface TimeTrackingV2Params extends QueryOptions {
  start_date: number;
  end_date: number;
  assignee: number;
}
export class TimeTrackingV2 extends ClickupEndpoint {
  getTimeEntriesWithinDateRange(taskId: string, query: TimeTrackingV2Params) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries`)
      .setQueryParams(query)
      .request('GET');
  }

  getSingularTimeEntry(teamId: string, timerId: string) {
    return this.httpClient
      .route(`/team/${teamId}/time_entries/${timerId}`)
      .request('GET');
  }

  getTimeEntryHistory(teamId: string, timerId: string) {
    return this.httpClient
      .route(`/team/${teamId}/time_entries/${timerId}/history`)
      .request('GET');
  }

  getRunningTimeEntry(
    teamId: string,
    query: Pick<TimeTrackingV2Params, 'assignee'>
  ) {
    return this.httpClient
      .route(`/team/${teamId}/time_entries/current`)
      .setQueryParams(query)
      .request('GET');
  }

  createTimeEntry(taskId: string, data: FormData, query: AttachmentsParams) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries`)
      .setQueryParams(query)
      .request('POST', data);
  }

  removeTagsFromTimeEntries(taskId: string) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/tags`)
      .request('DELETE');
  }

  getAllTagsFromTimeEntries(taskId: string) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/tags`)
      .request('GET');
  }
  addTagsFromTimeEntries(taskId: string, data: FormData) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/tags`)
      .request('POST', data);
  }
  changeTagNamesFromTimeEntries(taskId: string, data: FormData) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/tags`)
      .request('PUT', data);
  }
  startTimeEntry(
    taskId: string,
    timerId: string,
    data: FormData,
    query: AttachmentsParams
  ) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/start/${timerId}`)
      .setQueryParams(query)
      .request('POST', data);
  }
  stopTimeEntry(taskId: string) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/stop`)
      .request('POST');
  }
  deleteTimeEntry(taskId: string, timerId: string) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/${timerId}`)
      .request('DELETE');
  }
  updateTimeEntry(
    taskId: string,
    timerId: string,
    data: FormData,
    query: AttachmentsParams
  ) {
    return this.httpClient
      .route(`/team/${taskId}/time_entries/${timerId}`)
      .setQueryParams(query)
      .request('PUT', data);
  }
}
