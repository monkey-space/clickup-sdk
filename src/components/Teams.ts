import { ClickupEndpoint } from '../utils';
export class Teams extends ClickupEndpoint {
  getTeams() {
    return this.httpClient.route('/team').request('GET');
  }
}
