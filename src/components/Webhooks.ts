import { ClickupEndpoint } from '../utils';

export class Webhooks extends ClickupEndpoint {
  createWebhook(teamId: string | number, data: FormData) {
    return this.httpClient
      .route(`/team/${teamId}/webhook`)
      .request('POST', data);
  }
  updateWebhook(webhookId: string | number, data: FormData) {
    return this.httpClient.route(`/webhook/${webhookId}`).request('PUT', data);
  }
  deleteWebhook(webhookId: string | number) {
    return this.httpClient.route(`/webhook/${webhookId}`).request('DELETE');
  }
  getWebhook(teamId: string | number) {
    return this.httpClient.route(`/team/${teamId}/webhook`).request('GET');
  }
}
