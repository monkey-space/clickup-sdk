import { ClickupEndpoint } from '../utils';

/**
 * @deprecated Class not implemented!
 */
export class Dependencies extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  addDependency() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  deleteDependency() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  addTaskLink() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  deleteTaskLink() {
    throw new Error('Funtions not implemented!');
  }
}
