import { ClickupEndpoint } from '../utils';
export class Folders extends ClickupEndpoint {
  createFolder(spaceId: string | number, data: FormData) {
    return this.httpClient
      .route(`/space/${spaceId}/folder`)
      .request('POST', data);
  }
  updateFolder(folderId: string | number, data: FormData) {
    return this.httpClient.route(`/folder/${folderId}`).request('PUT', data);
  }
  deleteFolder(folderId: string | number) {
    return this.httpClient.route(`/folder/${folderId}`).request('DELETE');
  }
  getFolders(spaceId: string | number) {
    return this.httpClient.route(`/space/${spaceId}/folder`).request('GET');
  }
  getFolder(folderId: string | number) {
    return this.httpClient.route(`/folder/${folderId}`).request('GET');
  }
}
