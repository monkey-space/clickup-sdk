import { AttachmentsParams } from './Attachments';
import { ClickupEndpoint } from '../utils';
export enum TaskPriority {
  'Urgent' = 1,
  'High' = 2,
  'Normal' = 3,
  'Low' = 4,
}

export interface TasksParams extends AttachmentsParams {
  include_subtasks: boolean;
  archived: boolean;
  page: number;
  order_by: string;
  reverse: boolean;
  subtasks: boolean;
  statuses: Array<any>;
  include_closed: boolean;
  assignees: Array<any>;
  due_date_gt: number;
  due_date_lt: number;
  date_created_gt: number;
  date_created_lt: number;
  date_updated_gt: number;
  date_updated_lt: number;
  custom_fields: Array<any>;
  task_ids: string;
}
export class Tasks extends ClickupEndpoint {
  createTask(listId: string, data: FormData) {
    return this.httpClient.route(`/list/${listId}/task`).request('POST', data);
  }
  updateTask(taskId: string, data: FormData, query: AttachmentsParams) {
    return this.httpClient
      .route(`/task/${taskId}`)
      .setQueryParams(query)
      .request('PUT', data);
  }
  deleteTask(taskId: string, query: AttachmentsParams) {
    return this.httpClient
      .route(`/task/${taskId}`)
      .setQueryParams(query)
      .request('DELETE');
  }
  getTask(taskId: string, query: TasksParams) {
    return this.httpClient
      .route(`/task/${taskId}`)
      .setQueryParams(query)
      .request('GET');
  }
  getTasks(listId: string, query: Omit<TasksParams, 'include_subtasks'>) {
    return this.httpClient
      .route(`/list/${listId}/task`)
      .setQueryParams(query)
      .request('GET');
  }
  getFilteredTeamTasks(
    teamId: string,
    query: Omit<TasksParams, 'include_subtasks'>
  ) {
    return this.httpClient
      .route(`/team/${teamId}/task`)
      .setQueryParams(query)
      .request('GET');
  }
  getTasksTimeInStatus(taskId: string, query: AttachmentsParams) {
    return this.httpClient
      .route(`/task/${taskId}/time_in_status`)
      .setQueryParams(query)
      .request('GET');
  }
  getBulkTasksTimeInStatus(query: AttachmentsParams) {
    return this.httpClient
      .route(`/task/bulk_time_in_status/task_ids`)
      .setQueryParams(query)
      .request('GET');
  }
}
