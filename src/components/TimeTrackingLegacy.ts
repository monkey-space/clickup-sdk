import { ClickupEndpoint } from '../utils';

/**
 * @deprecated Class not implemented!
 */
export class TimeTrackingLegacy extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  trackTime() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  getTrackedTime() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  editTimeTracked() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  deleteTimeTracked() {
    throw new Error('Funtions not implemented!');
  }
}
