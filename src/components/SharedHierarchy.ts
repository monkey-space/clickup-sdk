import { ClickupEndpoint } from '../utils';

export class SharedHierarchy extends ClickupEndpoint {
  getSharedHierarchy(teamId: string | number) {
    return this.httpClient.route(`/team/${teamId}/shared`).request('GET');
  }
}
