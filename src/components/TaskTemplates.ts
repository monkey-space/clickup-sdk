import { ClickupEndpoint } from '../utils';
/**
 * @deprecated Class not implemented!
 */
export class TaskTemplates extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  getTaskTemplates() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  createTaskFromTemplate() {
    throw new Error('Funtions not implemented!');
  }
}
