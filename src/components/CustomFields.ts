import { ClickupEndpoint } from '../utils';

/**
 * @deprecated Class not implemented!
 */
export class CustomFields extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  setCustomFieldValue() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  removeCustomFieldValue() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  getAccessibleCustomFields() {
    throw new Error('Funtions not implemented!');
  }
}
