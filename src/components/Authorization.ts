import { QueryOptions, ClickupEndpoint } from './../utils/params';

export interface AuthorizationParams extends QueryOptions {
  client_id: string;
  client_secret: string;
  code: string;
}
export class Authorization extends ClickupEndpoint {
  async getAccessToken(queries: AuthorizationParams) {
    return this.httpClient
      .route('/oauth/token')
      .setQueryParams(queries)
      .request('GET');
  }

  async getAuthorizedUser() {
    return this.httpClient.route('/user').request('GET');
  }

  async getAuthorizedTeams() {
    return this.httpClient.route('/team').request('GET');
  }
}
