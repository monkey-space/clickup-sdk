import { ClickupEndpoint } from '../utils';

/**
 * @deprecated Class not implemented!
 */
export class Goals extends ClickupEndpoint {
  /**
   * @deprecated Funtions not implemented!
   */
  createGoal() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  updateGoal() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  deleteGoal() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  getGoals() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  getGoal() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  createKeyResult() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  editKeyResult() {
    throw new Error('Funtions not implemented!');
  }
  /**
   * @deprecated Funtions not implemented!
   */
  deleteKeyResult() {
    throw new Error('Funtions not implemented!');
  }
}
