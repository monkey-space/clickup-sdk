import { ClickupEndpoint } from '../utils';

export class Spaces extends ClickupEndpoint {
  createSpace(teamId: string | number, data: FormData) {
    return this.httpClient.route(`/team/${teamId}/space`).request('POST', data);
  }

  updateSpace(spaceId: string | number, data: FormData) {
    return this.httpClient.route(`/space/${spaceId}`).request('POST', data);
  }

  deleteSpace(spaceId: string | number) {
    return this.httpClient.route(`/space/${spaceId}`).request('DELETE');
  }

  getSpaces(teamId: string | number) {
    return this.httpClient.route(`/team/${teamId}/space`).request('GET');
  }

  getSpace(spaceId: string | number) {
    return this.httpClient.route(`/space/${spaceId}`).request('GET');
  }
}
