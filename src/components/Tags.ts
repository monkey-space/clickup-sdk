import { ClickupEndpoint } from '../utils';
export class Tags extends ClickupEndpoint {
  getSpaceTag() {}
  createSpaceTag() {}
  editSpaceTag() {}
  deleteSpaceTag() {}
  addTagToTask() {}
  removeTagFromTask() {}
}
